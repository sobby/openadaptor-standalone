package ru.cma.openadaptor.standalone;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Slf4jLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {
	
	static {
		try {
			Log.setLog(new Slf4jLog());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Autowired
	private MyBean myBean;
	
	public static void main(String[] args) throws Exception
	{
		ApplicationContext context =
			new ClassPathXmlApplicationContext("META-INF/jettyserver.xml");
		Main p = context.getBean(Main.class);
		p.start(args);
	}
	

	private void start(String[] args)
	{
		System.out.println("Bean started " + myBean.getIsStarted());
	}
}
